import { customElement } from 'lit-element/lib/decorators';
import { LitPage } from '../../utils/lit-page';
import { template } from './template';
import { changeUrl } from '@tjmonsi/lit-router';

/**
 * @type {LitPage}
 */
@customElement('page-logout')
class Page extends LitPage {
  render () {
    return template.bind(this)();
  }

  connectedCallback () {
    super.connectedCallback();

    this.logout();
  }

  async logout () {
    const { logout } = await import(
      /* webpackPreferch: true */
      /* webpackChunkName: "firebase-logout" */
      '../../utils/firebase/auth/main/logout'
    );

    await logout();

    changeUrl('/');
  }
}

export { Page };
