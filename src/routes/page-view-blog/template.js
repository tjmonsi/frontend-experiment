import { html } from 'lit-html';
import style from './style.component.scss';
import '../../components/blog/blog-loader';
import '../../components/blog/blog-component';

/**
 * @this {import('./index').Page & LitPage}
 */
export function template () {
  const { paramObject } = this;
  const { id } = paramObject;

  return html`
    <style>
      ${style}
    </style>

    <blog-loader .dataId="${id}">
      <blog-component></blog-component>
    </blog-loader>
  `;
}
