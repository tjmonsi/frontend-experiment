import { customElement, property } from 'lit-element/lib/decorators';
import { LitPage } from '../../utils/lit-page';
import { template } from './template';
import { worker, proxy } from '../../worker';
import { changeUrl } from '@tjmonsi/lit-router';

/**
 * @type {LitPage}
 */
@customElement('page-login')
class Page extends LitPage {
  /** @type {*} */
  @property()
  user = null;

  @property({ type: 'boolean' })
  userInitialized = false;

  constructor () {
    super();

    this._boundUpdateUser = this._updateUser.bind(this);
  }

  render () {
    return template.bind(this)();
  }

  connectedCallback () {
    super.connectedCallback();

    worker.auth.subscribe(proxy(this._boundUpdateUser));
  }

  disconnectedCallback () {
    worker.auth.unsubscribe(proxy(this._boundUpdateUser));
  }

  /**
   *
   * @param {*} user
   */
  _updateUser (user) {
    if (user) {
      changeUrl('/');
    }
  }

  async login () {
    const { login } = await import(
      /* webpackPreferch: true */
      /* webpackChunkName: "firebase-login" */
      '../../utils/firebase/auth/main/login'
    );

    await login();
    changeUrl('/');
  }
}

export { Page };
