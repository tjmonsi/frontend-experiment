import { worker } from '../../worker';

export const route = {
  component: 'page-edit-blog',
  preload: async () => {
    const { uid } = await worker.auth.getUser();
    if (!uid) {
      throw new Error('Not yet logged in');
    }
  },
  fallback: '/login',
  loader: () => import(
    /* webpackPreferch: true */
    /* webpackChunkName: "page-edit-blog" */
    './index')
};
