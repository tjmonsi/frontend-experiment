import { html } from 'lit-html';
import style from './style.component.scss';
import '../../components/blog/editor-component';
import '../../components/blog/blog-loader';

/**
 * @this {import('./index').Page & LitPage}
 */
export function template () {
  const { paramObject } = this;
  const { id, method } = paramObject;

  return html`
    <style>
      ${style}
    </style>

    <blog-loader .dataId="${id}" .method="${method}">
      <editor-component></editor-component>
    </blog-loader>
  `;
}
