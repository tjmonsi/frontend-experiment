import { LitElement } from 'lit-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { template } from './template';

/**
 * @type {LitElement}
 */
@customElement('editor-component')
class Component extends LitElement {
  @property()
  text = ''

  @property()
  titleHeader = ''

  @property()
  summary = ''

  @property()
  dataId = ''

  @property()
  isOwner = false;

  render () {
    return template.bind(this)();
  }

  /**
   *
   * @param {Event & CustomEvent} event
   */
  save (event) {
    event.preventDefault();

    const { target } = /** @type {*} */(event);
    const text = target.text.value;
    const title = target.title.value;
    const summary = target.summary.value;

    this.dispatchEvent(new window.CustomEvent('save-blog', {
      detail: {
        title,
        summary,
        text
      }
    }));
  }
}

export { Component };
