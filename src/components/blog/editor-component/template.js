import { html } from 'lit-html';
import style from './style.component.scss';

/**
 * @this {import('./index').Component}
 */
export function template () {
  const { text, titleHeader, summary, save, dataId } = this;

  return html`
    <style>
      ${style}
    </style>

    <form @submit="${save}">
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" id="title" name="title" value="${titleHeader}">
      </div>

      <div class="form-group">
        <label for="text">Summary:</label>
        <textarea class="summary" id="summary" name="summary">${summary}</textarea>
      </div>

      <div class="form-group">
        <label for="text">Content:</label>
        <textarea class="textarea" id="text" name="text">${text}</textarea>
      </div>

      <div class="button-group">
        ${dataId ? html`
          <a href="/blog/${dataId}">
            See blog
          </a>
        ` : ''}
        <button type="reset">
          Reset
        </button>
        <button>
          Save
        </button>

      </div>
    </form>

  `;
}
