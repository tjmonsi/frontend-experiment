import { UpdatingElement } from 'lit-element/lib/updating-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { changeUrl } from '@tjmonsi/lit-router';
import { worker } from '../../../worker';

@customElement('blog-loader')
class Component extends UpdatingElement {
  /** @type {string | undefined} */
  @property({ type: String })
  dataId;

  /** @type {string | undefined} */
  @property({ type: String })
  method;

  constructor () {
    super();

    this._boundSaveData = this.saveData.bind(this);
  }

  connectedCallback () {
    super.connectedCallback();

    for (const child of Array.from(this.childNodes)) {
      if (child && child.nodeType === 1) {
        child.addEventListener('save-blog', this._boundSaveData);
      }
    }
  }

  disconnectedCallback () {
    for (const child of Array.from(this.childNodes)) {
      if (child && child.nodeType === 1) {
        child.removeEventListener('save-blog', this._boundSaveData);
      }
    }
  }

  /**
   *
   * @param {*} changedProperties
   */
  updated (changedProperties) {
    super.updated(changedProperties);
    for (const key of changedProperties.keys()) {
      if (key === 'dataId' && this.dataId) {
        this._updateData(this.dataId);
      }
    }
  }

  /**
   *
   * @param {string} dataId
   */
  async _updateData (dataId) {
    const data = await worker.fetch(`blog/${dataId}?${this.method ? 'isOwner=true' : ''}`, {
      auth: !!this.method
    });
    if (data.success) {
      const { title, text, summary, id, owner } = data.data;

      const { uid } = await worker.auth.getUser();

      for (const child of Array.from(this.childNodes)) {
        const el =
          /** @type {import('../editor-component').Component} */(child);
        if (el && el.nodeType === 1) {
          el.titleHeader = title;
          el.text = text;
          el.summary = summary;
          el.dataId = id;
          el.isOwner = owner === uid;
        }
      }
    } else {
      changeUrl('/login');
    }
  }

  /**
   *
   * @param {Event} event
   */
  async saveData (event) {
    const { detail: body } = /** @type {CustomEvent} */(event);
    console.log(body);
    const response = await worker.fetch(this.dataId
      ? `blog/${this.dataId}`
      : 'blog', {
      method: this.dataId ? 'PUT' : 'POST',
      body,
      auth: true
    });

    if (response.success) {
      const { title, text, summary, id, owner } = response.data;

      const { uid } = await worker.auth.getUser();

      for (const child of Array.from(this.childNodes)) {
        const el =
          /** @type {import('../editor-component').Component} */(child);
        if (el && el.nodeType === 1) {
          el.titleHeader = title;
          el.text = text;
          el.summary = summary;
          el.dataId = id;
          el.isOwner = owner === uid;
        }
      }
    }
  }
}

export { Component };
