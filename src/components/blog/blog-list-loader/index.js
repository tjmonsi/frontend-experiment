import { UpdatingElement } from 'lit-element/lib/updating-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { worker } from '../../../worker';

@customElement('blog-list-loader')
class Component extends UpdatingElement {
  /** @type {string | undefined} */
  @property({ type: String })
  nextId;

  /** @type {string | undefined} */
  @property({ type: String })
  prevId;

  /** @type {[*?]} */
  data = [];

  connectedCallback () {
    super.connectedCallback();

    this._loadData();
  }

  /**
   *
   * @param {*} changedProperties
   */
  updated (changedProperties) {
    super.updated(changedProperties);
    for (const key of changedProperties.keys()) {
      if (key === 'nextId' && this.nextId) {
        this._loadData(this.nextId);
      }

      if (key === 'prevId' && this.prevId) {
        this._loadData(null, this.prevId);
      }
    }
  }

  /**
   *
   * @param {*} nextId
   * @param {*} prevId
   */
  async _loadData (nextId = null, prevId = null) {
    const limit = 2;
    let url = `blog?limit=${limit}`;
    if (nextId) {
      url = `${url}&nextId=${nextId}`;
    }
    if (prevId) {
      url = `${url}&prevId=${prevId}`;
    }
    const data = await worker.fetch(url);
    if (data.success && data.data && data.data.length) {
      this.data = data.data;

      for (const child of Array.from(this.childNodes)) {
        const el = /** @type {*} */(child);
        if (el && el.nodeType === 1) {
          el.data = this.data;
        }
      }
    }
  }
}

export { Component };
