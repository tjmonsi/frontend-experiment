import { LitElement } from 'lit-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { template } from './template';

/**
 * @type {LitElement}
 */
@customElement('blog-component')
class Component extends LitElement {
  @property()
  text = ''

  @property()
  titleHeader = ''

  @property()
  summary = ''

  /** @type {string | undefined} */
  @property()
  dataId;

  /** @type {boolean} */
  @property()
  isOwner = false;

  render () {
    return template.bind(this)();
  }
}

export { Component };
