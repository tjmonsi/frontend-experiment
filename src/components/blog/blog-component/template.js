import { html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import style from './style.component.scss';
import marked from 'marked';

/**
 * @this {import('./index').Component}
 */
export function template () {
  const { titleHeader, text, dataId, isOwner } = this;
  return html`
    <style>
      ${style}
    </style>

    <div id="main-container">
      ${titleHeader ? html`
      <h1>${titleHeader}</h1>
      ${unsafeHTML(marked(text))}
      ` : 'Loading...'}

      ${isOwner ? html`<a href="/blog/${dataId}/edit">Edit</a>` : ''}
    </div>

  `;
}
