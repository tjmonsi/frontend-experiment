import { html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import style from './style.component.scss';
import marked from 'marked';

/**
 * @this {import('./index').Component}
 */
export function template () {
  const { data } = this;
  const { title, summary, id } = data;
  return html`
    <style>
      ${style}
    </style>

    <div id="main-container">
      <h1><a href="/blog/${id}">${title}</a></h1>
      ${unsafeHTML(marked(summary))}
    </div>

  `;
}
