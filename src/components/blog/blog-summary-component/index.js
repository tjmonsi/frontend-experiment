import { LitElement } from 'lit-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { template } from './template';

/**
 * @type {LitElement}
 */
@customElement('blog-summary-component')
class Component extends LitElement {
  /** @type {*?} */
  @property({ type: Object })
  data;

  render () {
    return template.bind(this)();
  }
}

export { Component };
