import { html } from 'lit-html';
import { repeat } from 'lit-html/directives/repeat';
import style from './style.component.scss';
import '../blog-summary-component';

/**
 * @this {import('./index').Component}
 */
export function template () {
  const { data, nextId, prevId } = this;

  return html`
    <style>
      ${style}
    </style>

    <div class="main-container">
      ${data.length ? html`
        ${repeat(data, blog => blog.id, blog => html`
          <div>
            <blog-summary-component .data=${blog}></blog-summary-component>
          <!-- Blog: ${blog.id} - ${new Date(blog.dateCreated)} -->
          </div>
        `)}
      ` : 'Loading...'}

    </div>

    <div class="container">
      ${prevId ? html`
        <a href="?prevId=${prevId}">
          Previous
        </a>
      ` : ''}
      &nbsp;
      ${nextId ? html`
        <a href="?nextId=${nextId}">
          Next
        </a>
      ` : ''}
    </div>
  `;
}
