import { LitElement } from 'lit-element';
import { customElement, property } from 'lit-element/lib/decorators';
import { template } from './template';

/**
 * @type {LitElement}
 */
@customElement('blog-list-component')
class Component extends LitElement {
  /** @type {[*?]} */
  @property({ type: Array })
  data = [];

  /** @type {string | undefined} */
  @property({ type: String })
  nextId;

  /** @type {string | undefined} */
  @property({ type: String })
  prevId;

  render () {
    return template.bind(this)();
  }

  /**
   *
   * @param {*} changedProperties
   */
  updated (changedProperties) {
    super.updated(changedProperties);
    for (const key of changedProperties.keys()) {
      if (key === 'data' && this.data) {
        this._loadData(this.data);
      }
    }
  }

  /**
   *
   * @param {[*?]} data
   */
  _loadData (data) {
    if (data.length) {
      this.prevId = data[0].id;
      this.nextId = this.data[this.data.length - 1].id;
    }
  }
}

export { Component };
